import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_gphoto/flutter_gphoto_method_channel.dart';

void main() {
  MethodChannelFlutterGphoto platform = MethodChannelFlutterGphoto();
  const MethodChannel channel = MethodChannel('flutter_gphoto');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await platform.getPlatformVersion(), '42');
  });
}
