import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_gphoto/flutter_gphoto.dart';
import 'package:flutter_gphoto/flutter_gphoto_platform_interface.dart';
import 'package:flutter_gphoto/flutter_gphoto_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockFlutterGphotoPlatform 
    with MockPlatformInterfaceMixin
    implements FlutterGphotoPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final FlutterGphotoPlatform initialPlatform = FlutterGphotoPlatform.instance;

  test('$MethodChannelFlutterGphoto is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelFlutterGphoto>());
  });

  test('getPlatformVersion', () async {
    FlutterGphoto flutterGphotoPlugin = FlutterGphoto();
    MockFlutterGphotoPlatform fakePlatform = MockFlutterGphotoPlatform();
    FlutterGphotoPlatform.instance = fakePlatform;
  
    expect(await flutterGphotoPlugin.getPlatformVersion(), '42');
  });
}
