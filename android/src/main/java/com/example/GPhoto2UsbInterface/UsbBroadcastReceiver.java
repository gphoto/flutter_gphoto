package com.example.GPhoto2UsbInterface;

import android.content.Context;
import android.content.Intent;
import android.content.BroadcastReceiver;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.PluginRegistry.Registrar;

public class UsbBroadcastReceiver extends BroadcastReceiver {

    private MethodChannel mMethodChannel;

    public static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private UsbDevice mUsbDevice;

    public UsbBroadcastReceiver(MethodChannel channel) {
        mMethodChannel = channel;
    }

    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        if (ACTION_USB_PERMISSION.equals(action)) {

            synchronized (this) {
                mUsbDevice = (UsbDevice)intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                    if(mUsbDevice != null){
                        //call method to set up device communication
                        Log.d("DEBUG", "USB permission granted.");
                        mMethodChannel.invokeMethod("permissionCallback", true);
                    }
                }
                else {
                    Log.d("DEBUG", "USB permission denied.");
                    mMethodChannel.invokeMethod("permissionCallback", false);
                }
            }
        }
    }

}
