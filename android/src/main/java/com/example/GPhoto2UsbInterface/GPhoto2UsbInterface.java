package com.example.GPhoto2UsbInterface;

import android.content.Context;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbDevice;
import android.content.Intent;
import android.app.PendingIntent;
import android.content.IntentFilter;
import android.content.BroadcastReceiver;
import java.util.Iterator;
import java.util.HashMap;
import java.util.Collection;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.widget.Toast;
import android.util.Log;
import io.flutter.plugin.common.MethodChannel;

public class GPhoto2UsbInterface {

    static {

        System.loadLibrary("gphoto2_port");
    }


    private UsbManager mUsbManager;

    private UsbEndpoint mEp_in = null;
    private UsbEndpoint mEp_out = null;
    private UsbInterface mInterf = null;
    private UsbDeviceConnection mUsbDeviceConnection = null;
    private UsbDevice mUsbDevice = null;
    private PendingIntent permissionIntent = null;
    private int mUsbFileDescriptor = -1;
    private final BroadcastReceiver mUsbReceiver;
    private Context mContext = null;

    public GPhoto2UsbInterface(Context context, MethodChannel channel) {

        mContext = context;
        mUsbReceiver = new UsbBroadcastReceiver(channel);

        permissionIntent = PendingIntent.getBroadcast(mContext, 0, new Intent(UsbBroadcastReceiver.ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(UsbBroadcastReceiver.ACTION_USB_PERMISSION);
        mContext.registerReceiver(mUsbReceiver, filter);
    }

    public int getUsbFileDescriptor() {

        return mUsbFileDescriptor;
    }

    public boolean open() {
        
        if(!hasUsbPermission())
            return false;


        if(mUsbDevice == null) {
            return false;
        }

        boolean interfaceInitialized = setInterfacebEndpoints(mUsbDevice);


        if(interfaceInitialized && mInterf == null) {
            return false;
        }

        mUsbDeviceConnection = mUsbManager.openDevice( mUsbDevice );
        mUsbFileDescriptor = mUsbDeviceConnection.getFileDescriptor();
        boolean claimed = mUsbDeviceConnection.claimInterface(mInterf, true);

        if(claimed) {
            int ret = initUsb(this, mUsbFileDescriptor);
            int dummyLocation = 0;
            ret = initUsbWithId(mUsbDevice.getVendorId(), mUsbDevice.getProductId(), dummyLocation);
        }
        else {

            return false;
        }

        return true;
    }

    public void close() {

        if(mUsbDeviceConnection != null) {
            if(mInterf != null) {
                mUsbDeviceConnection.releaseInterface(mInterf);
            }

            mUsbDeviceConnection.close();
        }
    }

    public boolean hasUsbPermission() {

        if(mUsbDevice != null) {

            if(mUsbManager.hasPermission( mUsbDevice )) {
                return true;
            }
        }
        return false;
    }

    public void setUsbDevice() {
        if(mUsbDevice == null) {

            mUsbManager = (UsbManager) mContext.getSystemService(mContext.USB_SERVICE);
            HashMap< String, UsbDevice > stringDeviceMap    =       mUsbManager.getDeviceList();
            Collection< UsbDevice > usbDevices              = stringDeviceMap.values();

            Iterator< UsbDevice > usbDeviceIter = usbDevices.iterator();
            while( usbDeviceIter.hasNext() )
            {
                mUsbDevice = usbDeviceIter.next();
            }
        }
    }

    public boolean requestUsbPermission() {
        if(hasUsbPermission()) {
            
            return true;
        }


        setUsbDevice();

        if(mUsbDevice != null) {
          mUsbManager.requestPermission( mUsbDevice, permissionIntent );
        }

        return false;
    }

    public int controlTransfer(int requestType, int request, int value, int index, byte[] data, int length, int timeout) {

        int transf = controlTransfer(requestType, request, value, index, data, length, timeout);
        return transf;
    }

    public int bulkTransfereOut(byte[] data, int timeout) {

        int transf = 0;
        transf = mUsbDeviceConnection.bulkTransfer(mEp_out, data, data.length, timeout);
        return transf;
    }

    public int bulkTransfereIn(byte[] data, int size, int timeout) {
        
        if(data.length < size) {
            return -1;
        }
        
        int transf = mUsbDeviceConnection.bulkTransfer(mEp_in, data, size, timeout);
        return transf;
    }


    private boolean setInterfacebEndpoints(UsbDevice device) {
        
        if ( device != null && mInterf==null)
        {
            for(int c=0; c<device.getConfigurationCount(); c++) {
                android.hardware.usb.UsbConfiguration config = device.getConfiguration(c);
                for(int i=0; i<config.getInterfaceCount(); i++) {
                    mInterf = config.getInterface(i);
                    if(mInterf == null) {
                        continue;
                    }
                    for(int ep=0; ep<mInterf.getEndpointCount(); ep++) {
                        UsbEndpoint usbEndPoint = mInterf.getEndpoint(ep);
                        if(usbEndPoint.getType() == UsbConstants.USB_ENDPOINT_XFER_BULK) {
                            if(usbEndPoint.getDirection() == UsbConstants.USB_DIR_IN) {
                                Log.i("GPHOTO", "setInterfacebEndpoints - IN: " +ep);
                                mEp_in = mInterf.getEndpoint(ep);
                            }

                            if(usbEndPoint.getDirection() == UsbConstants.USB_DIR_OUT) {
                                Log.i("GPHOTO", "setInterfacebEndpoints - OUT: " +ep);
                                mEp_out = mInterf.getEndpoint(ep);
                            }
                        }
                    }
                }
            }

            if(mEp_in == null ||mEp_out == null || mInterf == null)
                return false;

            return true;
        }

        return false;
    }

    public native int initUsb( GPhoto2UsbInterface gp2, int fd);

    public native int initUsbWithId( int usbVendorID, int usbProductID, int usbLocationID);
}
