package com.example.flutter_gphoto;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import com.example.GPhoto2UsbInterface.GPhoto2UsbInterface;
import android.content.Context;
import android.util.Log;


public class FlutterGphotoController implements MethodCallHandler {

    private GPhoto2UsbInterface mGPhoto2UsbInterface = null;
    //static private Context mContext = null;

    public FlutterGphotoController(Context context, MethodChannel channel)
    {

        mGPhoto2UsbInterface = new GPhoto2UsbInterface(context, channel);
    }

    @Override
    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
        if (call.method.equals("getPlatformVersion")) {

            result.success("Android " + android.os.Build.VERSION.RELEASE);

            // } else if(call.method.equals("someOtherCase")) {
        } else if(call.method.equals("hasUsb")) {

          boolean hasUsb = mGPhoto2UsbInterface.hasUsbPermission();
          if(hasUsb) {

              result.success("has usb");
          }

          result.error("USB_ERROR", "usb not yet requested.", null);
        } else if(call.method.equals("requestUsb")) {

            boolean access = mGPhoto2UsbInterface.requestUsbPermission();
            if(access) {

                result.success("usb requested");
            }

            result.error("USB_ERROR", "usb request denied", null);

        } else if(call.method.equals("open")) {

            mGPhoto2UsbInterface.open();

            result.success("opened");
        } else if(call.method.equals("close")) {

            result.success("closed");
        } else {
            result.notImplemented();
        }
    }
}
