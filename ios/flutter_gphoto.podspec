#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint flutter_gphoto.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flutter_gphoto'
  s.version          = '0.0.1'
  s.summary          = 'A new Flutter plugin project.'
  s.description      = <<-DESC
A new Flutter plugin project.
                       DESC
  s.homepage         = 'https://gitlab.com/gphoto/flutter_gphoto'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'dev@jscheer.de' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = ['Classes/*.h', 'Classes/libgphoto_cmake/libgphoto2/**/*.h', 'Classes/libgphoto_cmake/libgphoto2_port/**/*.h']
  s.ios.frameworks = 'ImageCaptureCore'
  s.header_mappings_dir = 'Classes'
  s.xcconfig              = {
          'HEADER_SEARCH_PATHS' => [
              '"${PROJECT_DIR}"',
              '"${PROJECT_DIR}/Headers/Private/flutter_gphoto/libgphoto_cmake/libgphoto2"',
              '"${PROJECT_DIR}/Headers/Private/flutter_gphoto/libgphoto_cmake/libgphoto2_port"',
              '"${PROJECT_DIR}/Headers/Private/flutter_gphoto/libgphoto_cmake/iolibs/usb1/include"',
              '"${PROJECT_DIR}/Headers/Private/flutter_gphoto/libgphoto_cmake/include"',
              '"${PROJECT_DIR}/Headers/Private/flutter_gphoto/libgphoto_cmake/include/libusb-1.0"',
              '"${PROJECT_DIR}/Headers/Private/flutter_gphoto/libgphoto_cmake/camlibs/include"',

              '"${PROJECT_DIR}/Pods/Headers/Private/flutter_gphoto/libgphoto_cmake/libgphoto2"',
              '"${PROJECT_DIR}/Pods/Headers/Private/flutter_gphoto/libgphoto_cmake/libgphoto2_port"',
              '"${PROJECT_DIR}/Pods/Headers/Private/flutter_gphoto/libgphoto_cmake/iolibs/usb1/include"',
              '"${PROJECT_DIR}/Pods/Headers/Private/flutter_gphoto/libgphoto_cmake/include"',
              '"${PROJECT_DIR}/Pods/Headers/Private/flutter_gphoto/libgphoto_cmake/include/libusb-1.0"',
              '"${PROJECT_DIR}/Pods/Headers/Private/flutter_gphoto/libgphoto_cmake/camlibs/include"'
          ],
          'GCC_PREPROCESSOR_DEFINITIONS' => '_GPHOTO2_INTERNAL_CODE IOLIB_LIST=USB1 CAMLIBS=ptp2 HAVE_LIMITS_H',
          'HEADERMAP_INCLUDES_FLAT_ENTRIES_FOR_TARGET_BEING_BUILT' => 'NO'
      }
  s.exclude_files = [
    'Classes/libgphoto_cmake/**/*.txt',
    'Classes/libgphoto_cmake/**/*.out',
    'Classes/libgphoto_cmake/**/*.make',
    'Classes/libgphoto_cmake/**/*.cmake',
    'Classes/libgphoto_cmake/**/*.ptp2',
    'Classes/libgphoto_cmake/**/*.md',
    'Classes/libgphoto_cmake/**/*.o',
    'Classes/libgphoto_cmake/**/*.so',
    'Classes/libgphoto_cmake/**/*.log',
    'Classes/libgphoto_cmake/examples/**/*',
    'Classes/libgphoto_cmake/COPYING',
    'Classes/libgphoto_cmake/camlibs/src/cameras/olympus-x450',
    'Classes/libgphoto_cmake/ios/**/*',
    'Classes/libgphoto_cmake/libgphoto2/src/gphoto2-library.c',
    'Classes/libgphoto_cmake/libgphoto2_port/src/gphoto2-port-version.c',
    'Classes/libgphoto_cmake/iolibs/usb1/src/libusb1.c',
    'Classes/libgphoto_cmake/iolibs/usb1/src/libusb1JNI.c',
  ]
  s.dependency 'Flutter'
  s.platform = :ios, '13.0'

  # Flutter.framework does not contain a i386 slice. Only x86_64 simulators are supported.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'VALID_ARCHS[sdk=iphonesimulator*]' => 'x86_64' }
end
