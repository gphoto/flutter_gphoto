// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#import "FlutterGphotoPlugin.h"
#import "CaptureManager.h"

//@class ImageCaptureDeviceManagerImpl;

@implementation FlutterGphotoPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
      methodChannelWithName:@"flutter_gphoto"
            binaryMessenger:[registrar messenger]];
  FlutterGphotoPlugin* instance = [[FlutterGphotoPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if ([@"getPlatformVersion" isEqualToString:call.method]) {
    result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
  }
  else if ([@"open" isEqualToString:call.method]) {
      
      // create and start capture manager
      NSLog(@"open: trigger IOS 122123 ");
      _manager = [[CaptureManager alloc] init];
      _manager.delegate = self;
      [_manager startCamera];
      
      
    //result([@"iOS 123" stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);

    //ImageCaptureDeviceManagerImpl* manager = [[ImageCaptureDeviceManagerImpl alloc] init];
      
//      ImageCaptureDeviceManager* manger = new ImageCaptureDeviceManager();
//
//      NSArray<ICDevice *> * devs = manger->getDevices();
//      if(devs.count > 0)
//      {
//          ICDevice * firstDevice = devs.firstObject;
//
//      }
      
    NSString *resultVal = @"camera started";
    result(resultVal);
  } else {
    result(FlutterMethodNotImplemented);
  }
}

#pragma mark - CaptureManagerDelegate
-(void)cameraStarted {
    
    NSLog(@"Camera Example");
    
    Camera* mCamera;
    GPContext* mContext;
    CameraFile* mFile;

    int retval = gp_camera_new(&mCamera);
    mContext = gp_context_new();
    
    retval = gp_camera_init_libusb(mCamera, mContext);
    retval = gp_file_new(&mFile);

    NSLog(@"Camera Example 2");
    // simple camera trigger
    retval = gp_camera_trigger_capture (mCamera, mContext);


    gp_camera_exit(mCamera, mContext);
    
    NSLog(@"camera Stuff Finished");
}

@end
