//
//  CaptureManager.m
//  GPhotoTestApp
//
//  Created by Jonas Scheer on 30.07.20.
//  Copyright © 2020 Jonas Scheer. All rights reserved.
//

#import "CaptureManager.h"
//#import "CaptureManagerInterface.h"
#define PTP_RC_OK_OSX                       0x2001


void setPtpSendCallback(int (*ptpSendCallback)(void*, unsigned char*, unsigned char*, int, int), void* icCameraDevice);
void setPtpResonseCallback(int (*ptpResponseCallback)( unsigned char*, unsigned char*));
void setResetPtpResonseCallback(int (*resetPtpResponseCallback)());
void setCheckPtpResonseCallback(int (*checkPtpResponseCallback)(int*, int*));
int initUsb(int usbVendorID, int usbProductID, int usbLocationID);

ICCameraDevice* _camera = nil;

dispatch_semaphore_t ptpResponseSemaphore;

NSMutableData* responseCommand = nil;
NSMutableData* responseData = nil;
NSInteger responseLength = 0;
NSInteger dataLength = 0;

@implementation CaptureManager

- (instancetype)init {
    if ((self = [super init])) {
         NSLog(@"init");
    }
    
    return self;
}

- (void)close {
    [_deviceBrowser setDelegate:nil];
    [_deviceBrowser stop];
}

- (void)deviceDidBecomeReady:(ICDevice *)device {
    NSLog(@"deviceDidBecomeReady");
}

- (void)deviceBrowser:(ICDeviceBrowser*)browser
         didAddDevice:(ICDevice*)addedDevice
           moreComing:(BOOL)moreComing {
    
    if([addedDevice.productKind isEqualToString:@"Camera"]) {
        
        if ( [addedDevice.capabilities containsObject:ICCameraDeviceCanAcceptPTPCommands] )
        {
            ICCameraDevice* camera = (ICCameraDevice *) addedDevice;
        
            
            camera.delegate = self;
            [camera requestOpenSession];
            
            _camera = camera;
            bool isTehter = _camera.tetheredCaptureEnabled;
            
            setPtpSendCallback(externPtpCommand, (__bridge void*)_camera);
            setPtpResonseCallback(getPtpResponse);
            setCheckPtpResonseCallback(checkPtpResponse);
            setResetPtpResonseCallback(resetPtpResponse);

            ptpResponseSemaphore = dispatch_semaphore_create(0);
        }
    }
}

- (void)deviceBrowser:(nonnull ICDeviceBrowser *)browser didRemoveDevice:(nonnull ICDevice *)device moreGoing:(BOOL)moreGoing {
}


- (void)device:(nonnull ICDevice *)device didCloseSessionWithError:(NSError * _Nullable)error {
    NSLog(@"READY: didCloseSessionWithError");
}


- (void)device:(nonnull ICDevice *)device didOpenSessionWithError:(NSError * _Nullable)error {
    
    if ( error )
        NSLog(@"\nFailed to open a session on '%@'.\nError: '%@' \n", device.name, error.description);
    else
        NSLog(@"\nSession opened on '%@'.\n", device.name);
}


- (void)didRemoveDevice:(nonnull ICDevice *)device {
    if([device.productKind isEqualToString:@"Camera"]) {
        {
        }
    }
}

static int externPtpCommand(void* icCameraDevice, unsigned char* command, unsigned char* data, int commandSize, int dataSize)
{
    NSLog(@ "len %d\n", *((uint32_t*)(command)));
    NSLog(@ "phase %hu\n", *((uint16_t*)(command+4)));
    NSLog(@ "code 0x%04hX\n", *((uint16_t*)(command+6)));
    NSLog(@ "Transaction_ID %d\n", *((uint32_t*)(command+8)));
    NSLog(@ "Param1 ggg %d\n", *((uint32_t*)(command+12)));
    
    
    
    // Prepare data to be send using requestSendPTPCommand()
    NSData* sendCommand = [NSData dataWithBytesNoCopy:command length:commandSize freeWhenDone:NO];
    NSData* sendData = [NSData dataWithBytesNoCopy:data length:dataSize freeWhenDone:NO];

    
    
    ICCameraDevice* cam = (__bridge ICCameraDevice*)icCameraDevice;
    bool openSession = cam.hasOpenSession;
    [cam requestSendPTPCommand:sendCommand outData:sendData completion:^(NSData* d1, NSData* d2, NSError* err){

        NSLog(@"PTP: completion");
        NSLog(@"\nERROR:  '%@'.\n", err.localizedDescription);
        
        dataLength = d1.length;
        responseLength = d2.length;
        
        // ensure that storage is allocated and can hold enough data
        if(responseCommand == nil) {
           responseCommand = [NSMutableData dataWithLength:sizeof(unsigned char) * 12];
        }
        if(responseCommand.length < responseLength )
        {
            [responseCommand setLength:sizeof(unsigned char) * responseLength];
        }
        
        if(responseData == nil) {
           responseData = [NSMutableData dataWithLength:sizeof(unsigned char) * 4096];
        }
        if(responseData.length < dataLength )
        {
            [responseData setLength:sizeof(unsigned char) * dataLength];
        }
        
        // set data
        [responseCommand setData:d2];
        [responseData setData:d1];
        
        dispatch_semaphore_signal(ptpResponseSemaphore);
    }];
    
    NSLog(@ "END\n");
    
    return commandSize;
}

static int checkPtpResponse(int* commandSize, int* dataSize)
{
    NSLog(@"PTP: checkPtpResponse 1");
    if (![NSThread isMainThread]) {
        dispatch_semaphore_wait(ptpResponseSemaphore, DISPATCH_TIME_FOREVER);
    } else {
        while (dispatch_semaphore_wait(ptpResponseSemaphore, DISPATCH_TIME_NOW)) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:0]];
        }
    }
    
    NSLog(@"PTP: checkPtpResponse 2");
    
    *commandSize = (int)responseLength;
    *dataSize = (int)dataLength;
    
    return PTP_RC_OK_OSX;
}

static int getPtpResponse(unsigned char* command, unsigned char* data)
{
    NSLog(@"PTP: getPtpResponse");
    memcpy(command, responseCommand.bytes, responseLength);
    memcpy(data, responseData.bytes, dataLength);
    
    return PTP_RC_OK_OSX;
}

static int resetPtpResponse()
{
    NSLog(@"PTP: resetPtpResponse");
    dispatch_semaphore_signal(ptpResponseSemaphore);
    return PTP_RC_OK_OSX;
}

//- (void)requestControlAuthorizationWithCompletion:(void (^)(ICAuthorizationStatus status))completion
//{
//
//}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didAddItems:(nonnull NSArray<ICCameraItem *> *)items {
    NSLog(@"READY: didAddItems");
}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didReceiveMetadata:(NSDictionary * _Nullable)metadata forItem:(nonnull ICCameraItem *)item error:(NSError * _Nullable)error {
    NSLog(@"READY: didReceiveMetadata");
}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didReceivePTPEvent:(nonnull NSData *)eventData {
    NSLog(@"READY: didReceivePTPEvent");
}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didReceiveThumbnail:(CGImageRef _Nullable)thumbnail forItem:(nonnull ICCameraItem *)item error:(NSError * _Nullable)error {
    NSLog(@"READY: didReceiveThumbnail");
}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didRemoveItems:(nonnull NSArray<ICCameraItem *> *)items {
    NSLog(@"READY: didRemoveItems");
}

- (void)cameraDevice:(nonnull ICCameraDevice *)camera didRenameItems:(nonnull NSArray<ICCameraItem *> *)items {
    NSLog(@"READY: didRenameItems");
}

- (void)cameraDeviceDidChangeCapability:(nonnull ICCameraDevice *)camera {
    NSLog(@"READY: cameraDeviceDidChangeCapability");
}

- (void)cameraDeviceDidEnableAccessRestriction:(nonnull ICDevice *)device {
    NSLog(@"READY: cameraDeviceDidEnableAccessRestriction");
}

- (void)cameraDeviceDidRemoveAccessRestriction:(nonnull ICDevice *)device {
    NSLog(@"READY: cameraDeviceDidRemoveAccessRestriction");
}

- (void)deviceDidBecomeReadyWithCompleteContentCatalog:(nonnull ICCameraDevice *)device {
    NSLog(@"READY: deviceDidBecomeReadyWithCompleteContentCatalog");
    
    if(device != _camera)
        return;
    
    int usbVendor = _camera.usbVendorID;
    int usbProduct = _camera.usbProductID;
    initUsb(usbVendor, usbProduct, _camera.usbLocationID);
    
    [_delegate cameraStarted];
}


-(void)startCamera
{
    _deviceBrowser = [[ICDeviceBrowser alloc] init];
    _deviceBrowser.delegate = self;
    
    [_deviceBrowser start];
}

@end
