// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <GphotoCameraOption.h>
#include <gphotoTools.h>

GphotoCameraOption::GphotoCameraOption(Camera* camera, GPContext* context, std::string widgetPath)
{
	mCamera = camera;
	mContext = context;
	mWidgetPath = splitPath(widgetPath);
}

std::vector<std::string> GphotoCameraOption::splitPath(std::string widgetPath)
{
	size_t startPos = 0;
	size_t pos = widgetPath.find("/", startPos);
	std::vector<std::string> wPath;

	while(pos != std::string::npos)
	{
		std::string subStr = widgetPath.substr(startPos, pos-startPos);
		if(subStr.size() > 0)
		{
			wPath.push_back(subStr);
		}
		
		startPos = pos+1;
		if(startPos >= widgetPath.size()) {
			break;
		}

		pos = widgetPath.find("/", startPos);
	}

	if(startPos < widgetPath.size()-1){
		std::string subStr = widgetPath.substr(startPos, widgetPath.size()-startPos);
		wPath.push_back(subStr);
	}

	return wPath;
}

void GphotoCameraOption::setValue(const char* value)
{
	CameraWidget *widget = getCameraWidget();
	if(widget == nullptr)
		return;
	
	char *current;
	
	int retval = gp_widget_set_value(widget, value);
	if (retval != GP_OK) {
		gp_widget_free(widget);
		printf("GP_ERROR: gp_widget_set_value(%s)", value);
	}

	updateCameraConfig();	
	gp_widget_free(widget);
}

char* GphotoCameraOption::getValue()
{
	CameraWidget *widget = getCameraWidget();
	if(widget == nullptr) {
		gp_widget_free(widget);
		return nullptr;
	}
		
	char *current;
	int retval = gp_widget_get_value(widget, &current);

	if (retval != GP_OK) {
		gp_widget_free(widget);
		return nullptr;
	}

	gp_widget_free(widget);
	return current;
}

int GphotoCameraOption::getOptionCount()
{
	get_options();
	return options.size();
}

char* GphotoCameraOption::getOptionChoise(int index) 
{
	get_options();

	if(options.size() > index)
	{
		return options[index];
	}

	return nullptr;
}

std::vector<char*> GphotoCameraOption::get_options()
{
	if(options.size() > 0)
	{
		return options;
	}
	
	int retval;
	CameraWidget *widget = getCameraWidget();
	if(widget == nullptr) {
		gp_widget_free(widget);
		return options;
	}

	int choises = gp_widget_count_choices(widget);
	if(choises > 0) 
	{
		for(int i=0; i<choises; i++) {

			char *choice;
			retval = gp_widget_get_choice(widget, i, (const char **)&choice);			 // TODO
			if(retval == GP_OK) {
				char *newChoice = (char*)malloc(strlen(choice)+1);
				strcpy(newChoice, choice);
				options.push_back(newChoice);
			}
		}
	}

	gp_widget_free(widget);
	return options;
}

CameraWidget* GphotoCameraOption::getCameraWidget()
{
	int retval;

	if(mCamera == nullptr || mContext == nullptr) {
    	return nullptr;
  	}

	CameraWidget *rootconfig; // okay, not really
	CameraWidgetType widgettype;
	retval = gp_camera_get_config(mCamera, &rootconfig, mContext);
	if (retval != GP_OK) {
		return nullptr;
	}

	mActualRootConfig = rootconfig;

	for(int i=0; i<mWidgetPath.size(); i++) {
		const char* childName = mWidgetPath[i].c_str(); 


		CameraWidget *child;
		retval = gp_widget_get_child_by_name(rootconfig, childName, &child);
		if (retval != GP_OK) {
			std::string currentChildPath = "";
			for(int j=0; j<i; j++) {
				currentChildPath.append("/");
				currentChildPath.append(mWidgetPath[j]);
			}
			currentChildPath.append(childName);

			return nullptr;
		}

		gp_widget_get_type(child, &widgettype);
		rootconfig = child;
	}

	return rootconfig;
}

void GphotoCameraOption::updateCameraConfig() 
{
	if(mActualRootConfig == nullptr)
		return;

	gp_camera_set_config(mCamera, mActualRootConfig, mContext);
}