// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <libgphotoFFI.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <gphotoTools.h>

DART_EXPORT int64_t gp_init()
{
  std::lock_guard<std::mutex> lock(mGPhotoMutex);

  int64_t retval = 0;
  retval = gp_camera_new(&mCamera);
  if (retval != GP_OK) {

    return retval;
  }

  mContext = gp_context_new();
  if(mContext == nullptr)
    return GP_ERROR;

  retval = gp_camera_init_libusb(mCamera, mContext);
  /* Note:
   * gp_camera_init_libusb might return an error, however consecutive commands still work.
   */

  retval = gp_file_new(&mFile);
  return GP_OK;
}

DART_EXPORT int64_t gp_exit()
{

  if(mCamera == nullptr || mContext == nullptr) {

    return GP_ERROR;
  }

  mCameraOptions.clear();

  int retval;
  {
    std::lock_guard<std::mutex> lock(mGPhotoMutex);
    retval = gp_camera_exit (mCamera, mContext);
  }
  return retval;
}

DART_EXPORT int64_t gp_triggerCamera()
{
  if(mCamera == nullptr || mContext == nullptr) {
    return GP_ERROR;
  }
  int retval, nrcapture = 0;

  {
   std::lock_guard<std::mutex> lock(mGPhotoMutex);

    retval = gp_camera_trigger_capture (mCamera, mContext);
  }
  return retval;
}

DART_EXPORT uint64_t grabImage(uint8_t** data) {

  if(mCamera == nullptr || mContext == nullptr || mFile == nullptr) {
    return 0;
  }

  unsigned long size = 0;
  {
    std::lock_guard<std::mutex> lock(mGPhotoMutex);

    int retval = gp_camera_capture_preview(mCamera, mFile, mContext);
    if(retval != GP_OK) {
      return 0;
    }

    retval = gp_file_get_data_and_size(mFile, (const char **) data, &size);
    if(retval != GP_OK) {
      return 0;
    }

  }
  return (uint64_t)size;
}

DART_EXPORT uint64_t getNativeGrabImage() {

  uint64_t (* grabImageFunction)(uint8_t**) = &grabImage;

  return (uint64_t) grabImageFunction;
}

DART_EXPORT uint64_t gp_grabImage(uint8_t** data)
{
    if(mCamera == nullptr || mContext == nullptr || mFile == nullptr) {
        return 0;
    }

    unsigned long size;
    {
      std::lock_guard<std::mutex> lock(mGPhotoMutex);

      int retval = gp_camera_capture_preview(mCamera, mFile, mContext);
      if(retval != GP_OK)
          return 0;

      retval = gp_file_get_data_and_size(mFile, (const char **) data, &size);   // Do we need to lock this ?
      if(retval != GP_OK)
          return 0;
    }

    return (int64_t)size;
}

DART_EXPORT int64_t gp_set_capturetarget_sdcard() {

    int retval = gp_set_capturetarget("Memory card");
    return retval;
}

DART_EXPORT int64_t gp_set_capturetarget_internal() {

    int retval = gp_set_capturetarget("Internal RAM");
    return retval;
}

int64_t gp_set_capturetarget(const char* captureTarget)
{
  if(mCamera == nullptr || mContext == nullptr)
  {
    return 0;
  }

  std::lock_guard<std::mutex> lock(mGPhotoMutex);


  int retval;

  CameraWidget *rootconfig; // okay, not really

  CameraWidget *actualrootconfig;
  retval = gp_camera_get_config(mCamera, &rootconfig, mContext);
  if(retval != GP_OK)
      return 0;

  CameraWidget *child;
  retval = gp_widget_get_child_by_name(rootconfig, "main", &child);
  if(retval != GP_OK)
      return 0;

  actualrootconfig = child;

  rootconfig = child;
  retval = gp_widget_get_child_by_name(rootconfig, "settings", &child);
  if(retval != GP_OK)
      return 0;

  rootconfig = child;
  retval = gp_widget_get_child_by_name(rootconfig, "capturetarget", &child);
  if(retval != GP_OK)
      return 0;

  CameraWidget *capture = child;

  char *current;
  retval = gp_widget_set_value(capture, captureTarget);
  if(retval != GP_OK)
      return 0;

  retval = gp_camera_set_config(mCamera, actualrootconfig, mContext);
  if(retval != GP_OK)
      return 0;

  retval = gp_widget_get_value(capture, &current);
  if(retval != GP_OK)
      return 0;

  gp_widget_free (actualrootconfig);		// TODO: test

  return 1;
}

GphotoCameraOption* findCreateOption(char* optionName)
{
  std::string name(optionName);
  std::unordered_map<std::string, GphotoCameraOption*>::const_iterator iter = mCameraOptions.find(name);
  if(iter == mCameraOptions.end())
  {
    GphotoCameraOption* option = new GphotoCameraOption(mCamera, mContext, name);
    std::pair<std::string, GphotoCameraOption*> strToOpt(name, option);
    mCameraOptions.insert(strToOpt);
  }

  GphotoCameraOption* option = mCameraOptions[name];
  return option;
}

DART_EXPORT uint64_t gp_setCameraOption(char* optionName, char* value)
{

  GphotoCameraOption* option = findCreateOption(optionName);

  {
    std::lock_guard<std::mutex> lock(mGPhotoMutex);
    option->setValue(value);
  }

  return 88;
}

DART_EXPORT uint64_t gp_getCameraOption(char* optionName, char** value)
{

  if(mCamera == nullptr || mContext == nullptr) {
    return 0;
  }

  GphotoCameraOption* option = findCreateOption(optionName);

  char* val;
  {
    std::lock_guard<std::mutex> lock(mGPhotoMutex);
    val = option->getValue();
  }

  if(val == nullptr)
    return 0;

  *value = val;

  return 1;
}

DART_EXPORT uint64_t gp_getCameraOptionCount(char* optionName)
{
  GphotoCameraOption* option = findCreateOption(optionName);

  int count;
  {
  std::lock_guard<std::mutex> lock(mGPhotoMutex);
  count = option->getOptionCount();

  }

  return count;
}

DART_EXPORT uint64_t gp_getCameraOptionChoice(char* optionName, char** value, uint64_t index)
{
  GphotoCameraOption* option = findCreateOption(optionName);

  char* val = nullptr;
  {
    std::lock_guard<std::mutex> lock(mGPhotoMutex);
    val = option->getOptionChoise(index);

  }

  if(val == nullptr) {
    return 0;
  }

  *value = val;
  return 1;
}

DART_EXPORT uint64_t gp_getAllImageNames(char*** folderNames, char*** imgNames, uint64_t* imgCount)
{
  std::vector<std::string> folderNamesStr;
	std::vector<std::string> imgNamesStr;
  *imgCount = 0;

	std::vector<std::string> folders;
	int retval = getFolders("/", folders, true);
	if(retval != GP_OK) {
		return retval;
	}
	for(int i=0; i<folders.size(); i++) {

		std::vector<std::string> imgs;
		retval = getImages(folders[i].c_str(), imgs);
		if(retval == GP_OK) {
			for(int j=0; j<imgs.size(); j++) {

				folderNamesStr.push_back(folders[i]);
				imgNamesStr.push_back(imgs[j]);
			}
		}
	}

	*imgCount = folderNamesStr.size();
	*folderNames = (char**)malloc(sizeof(char*) * *imgCount);
	*imgNames = (char**)malloc(sizeof(char*) * *imgCount);
	for(int i=0; i<folderNamesStr.size(); i++) {
		size_t len;

		len = strlen(folderNamesStr[i].c_str());

		char* folderName = (char*)malloc(sizeof(char)*len);
		strcpy(folderName, folderNamesStr[i].c_str());
		(*folderNames)[i] = folderName;
		len = strlen(folderNamesStr[i].c_str());
		char* imgName = (char*)malloc(sizeof(char)*len);
		strcpy(imgName, imgNamesStr[i].c_str());
		(*imgNames)[i] = imgName;
	}

	return 0;
}

int getFolders(const char* startFolder, std::vector<std::string> &folders, bool recursive)
{
  int retval;
	CameraList *list;
  {
    std::lock_guard<std::mutex> lock(mGPhotoMutex);

    retval = gp_list_new (&list);
    retval = gp_camera_folder_list_folders(mCamera, startFolder, list, mContext);
    if(retval != GP_OK) {
      gp_list_free (list);
      return retval;
    }
  }

  {
    mGPhotoMutex.lock();

    int count = gp_list_count (list);
    for (int i = 0; i < count; i++) {
      const char *name;
      gp_list_get_name (list, i, &name);
      std::string folderName(name);
      std::string fullFolderPath(startFolder);
      std::string lastChar = fullFolderPath.substr(fullFolderPath.length()-1, fullFolderPath.length());
      if(lastChar.compare("/") != 0) {
        fullFolderPath.append("/");
      }
      fullFolderPath.append(folderName);
      folders.push_back(fullFolderPath);
      if(recursive) {
        mGPhotoMutex.unlock();
        getFolders(fullFolderPath.c_str(), folders, recursive);
      }
    }

    mGPhotoMutex.unlock();
    gp_list_free (list);
  }
	return (GP_OK);
}

int getImages(const char* startFolder, std::vector<std::string> &imgNames)
{
  int retval;
	CameraList *list;

  {
    std::lock_guard<std::mutex> lock(mGPhotoMutex);
    retval = gp_list_new (&list);
    retval = gp_camera_folder_list_files(mCamera, startFolder, list, mContext);
    if(retval != GP_OK) {
      gp_list_free (list);
      return retval;
	  }
  }

  int count;
  {
    std::lock_guard<std::mutex> lock(mGPhotoMutex);
	  count = gp_list_count (list);

    for (int i = 0; i < count; i++) {
      const char *name;
      gp_list_get_name (list, i, &name);
      std::string imgName(name);
      imgNames.push_back(imgName);
    }

    gp_list_free (list);
  }
	return (GP_OK);
}

DART_EXPORT uint64_t gp_getImageByName(char* folderName, char* imgName, uint8_t** imgData, uint64_t* imgSize, uint8_t cameraFileType, uint32_t copyData)
{
  int retval;

  if(copyData == 0)
  {
    {
      std::lock_guard<std::mutex> lock(mGPhotoMutex);
      retval = gp_file_new(&getImageByName_file);
      if (retval != GP_OK) {
        return retval;
      }

      CameraFileType type = getFileType(cameraFileType);
      retval = gp_camera_file_get(mCamera, folderName, imgName, type, getImageByName_file, mContext);
      if (retval != GP_OK) {

        if(type == GP_FILE_TYPE_NORMAL) {
          retval = gp_camera_file_get(mCamera, folderName, imgName, GP_FILE_TYPE_RAW, getImageByName_file, mContext);
        }
        if(type == GP_FILE_TYPE_RAW) {
          retval = gp_camera_file_get(mCamera, folderName, imgName, GP_FILE_TYPE_NORMAL, getImageByName_file, mContext);
        }

        if (retval != GP_OK) {
          gp_print("gp_camera_file_get: %s, %s, %d", folderName, imgName, type);
          return retval;
        }
      }

      retval = gp_file_get_data_and_size (getImageByName_file, (const char**)&getImageByName_cache, &getImageByName_cacheSize);
    }

    if (retval != GP_OK) {
      return retval;
	  }

    *imgSize = getImageByName_cacheSize;
    return (GP_OK);
  }

  memcpy(*imgData, getImageByName_cache, getImageByName_cacheSize);
  {
    std::lock_guard<std::mutex> lock(mGPhotoMutex);
    gp_file_free(getImageByName_file);
  }

  return (GP_OK);
}

CameraFileType getFileType(int intVal)
{
  
  if(intVal == 0)
    return GP_FILE_TYPE_PREVIEW;

  if(intVal == 1)
    return GP_FILE_TYPE_NORMAL;

  if(intVal == 2)
    return GP_FILE_TYPE_RAW;

  if(intVal == 3)
    return GP_FILE_TYPE_AUDIO;

  if(intVal == 4)
    return GP_FILE_TYPE_EXIF;

  if(intVal == 5)
    return GP_FILE_TYPE_METADATA;

  return GP_FILE_TYPE_NORMAL;
}
