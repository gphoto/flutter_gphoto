// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FLUTTER_PLUGIN_FLUTTERGPHOTO_FFI_H
#define FLUTTER_PLUGIN_FLUTTERGPHOTO_FFI_H

#ifdef __APPLE__
    //#include <tr1/cstdint>
#else
    #include <cstdint>
#endif

#include <unordered_map>
#include "GphotoCameraOption.h"
#include <gphoto2/gphoto2-camera.h>
#include <mutex>

#ifdef __ANDROID__
//#include <jni.h>
#include <android/log.h>
#define  LOG_TAG    "GPHOTO2"
#endif

#if __cplusplus

#if defined(_WIN32)
#define DART_EXPORT extern "C" __declspec(dllexport)
#else
#define DART_EXPORT                                                            \
  extern "C" __attribute__((visibility("default"))) __attribute((used))
#endif

static int gp_print(const char *fmt, ...);

DART_EXPORT int64_t gp_init();

DART_EXPORT int64_t gp_exit();

DART_EXPORT int64_t gp_set_capturetarget_sdcard();

DART_EXPORT int64_t gp_set_capturetarget_internal();

DART_EXPORT int64_t gp_triggerCamera();

DART_EXPORT uint64_t gp_grabImage(uint8_t** data);

DART_EXPORT uint64_t grabImage(uint8_t** data);

/**
 * @brief Returns an pointer to the grabImage function as 64 bit unsigned integer.
 *
 * The grabImage function has the signature: uint64_t grabImage(uint8_t** data)
 * It uses, gp_camera_capture_preview() and gp_file_get_data_and_size() for retreiving
 * image data. gp_init() must be called in advance.
 *
 * @return DART_EXPORT function pointer, casted to 64 bit unsigned integer.
 */
DART_EXPORT uint64_t getNativeGrabImage();

DART_EXPORT uint64_t gp_setCameraOption(char* optionName, char* value);

DART_EXPORT uint64_t gp_getCameraOption(char* optionName, char** value);

DART_EXPORT uint64_t gp_getCameraOptionCount(char* optionName);

DART_EXPORT uint64_t gp_getCameraOptionChoice(char* optionName, char** value, uint64_t index);

DART_EXPORT uint64_t gp_getAllImageNames(char*** folderNames, char*** imgNames, uint64_t* imgCount);

int getFolders(const char* startFolder, std::vector<std::string> &folders, bool recursive);

int getImages(const char* startFolder, std::vector<std::string> &imgNames);

uint8_t* getImageByName_cache;    // pointer to storage, managed by gPhoto2
unsigned long getImageByName_cacheSize;
CameraFile *getImageByName_file = nullptr;
DART_EXPORT uint64_t gp_getImageByName(char* folderName, char* imgName, uint8_t** imgData, uint64_t* imgSize, uint8_t cameraFileType, uint32_t copyData);

CameraFileType getFileType(int intVal);

GphotoCameraOption* findCreateOption(char* optionName);

std::mutex mGPhotoMutex;

Camera* mCamera = nullptr;
GPContext* mContext = nullptr;
CameraFile* mFile = nullptr;


// general camera settings

int64_t gp_set_capturetarget(const char* captureTarget);

// camera settings

std::unordered_map<std::string, GphotoCameraOption*> mCameraOptions;

// GphotoCameraSettings

#endif

#endif // FLUTTER_PLUGIN_FLUTTERGPHOTO_FFI_H
