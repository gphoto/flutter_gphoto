// Copyright 2020 The Lensshift Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef FLUTTER_PLUGIN_FLUTTERGPHOTO_GPCAMOPTION_H
#define FLUTTER_PLUGIN_FLUTTERGPHOTO_GPCAMOPTION_H

#ifdef __APPLE__
    //#include <tr1/cstdint>
#else
    #include <cstdint>
#endif

#include <string>
#include <vector>
#include <gphoto2/gphoto2-camera.h>

class GphotoCameraOption {

public:

  GphotoCameraOption(Camera* camera, GPContext* context, std::string widgetPath);

  void setValue(const char* value);

  char* getValue();

  int getOptionCount();

  char* getOptionChoise(int index);

  std::vector<char*> get_options();

private:

  CameraWidget* getCameraWidget();

  void updateCameraConfig();

  std::vector<std::string> splitPath(std::string widgetPath);

  std::vector<char*> options;

  std::vector<std::string> mWidgetPath;

  Camera* mCamera = nullptr;

  GPContext* mContext = nullptr;

  CameraWidget *mActualRootConfig = nullptr;
};

#endif // FLUTTER_PLUGIN_FLUTTERGPHOTO_GPCAMOPTION_H
